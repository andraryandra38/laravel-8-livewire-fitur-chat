<div class="container">
    <div class="row">
        <div class="col-md-4 mb-4">
            <div class="card">
                <div class="card-header">
                    Groups
                </div>
                <div class="card-body">
                    <div class="list-group list-group-flush">
                        @foreach ($groups as $item)
                        <div wire:click="selectGroup({{ $item->id }})" role="button" class="list-group-item text-capitalize">
                            
                            {{ $item->name }} 

                        </div>  
                        @endforeach
                     </div>
                </div>
            </div>
        </div>
        @if ($group != NULL)
        <div class="col-md-8">
            <div class="card">
                <div class="card-header text-capitalize">{{ $group->name }}</div>
                    <div class="card-body">
                        <div wire:poll class="list-group list-group-flush mb-4 overflow-auto " style="max-height: 700px; display: flex; flex-direction:column-reverse;">
                            @foreach ($chats as $item)
                            <div  class="list-group-item p-2 m-1
                            @if($item->user_id == $user->id) bg-primary text-end text-light  @endif" 
                            @if($item->user_id != $user->id) style="color:#2CFF9F; background-color: #252525;" @endif>

                                <div class="">[{{ $item->created_at }}] {{ $item->user->name }}</div>
                                <div class="text-capitalize text-light">{{ $item->message }}</div>
                            </div> 
                            @endforeach
                            
                            
                     </div>
                     <div class="d-flex">
                         <input wire:model="my_text" type="text" class="form-control me-3 @error('my_text') is-invalid @enderror" placeholder="Messages...">
                         <button wire:click="send" class="btn btn-primary">Send</button>
                     </div>
                </div>
            </div>
        </div> 
        @endif
        
    </div>
</div>